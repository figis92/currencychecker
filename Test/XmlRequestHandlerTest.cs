﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XmlCurrencyRateManager;

namespace Test
{
    [TestClass]
    public class XmlRequestHandlerTest
    {
        [TestMethod]
        public void Test_ExchangeRateGet_DateOutOfScopeOfService()
        {
            //arrange
            var date = DateTime.Now;
            //act
            var rates = XmlRequestHandler.GetExchangeRateDataByDate(date);
            //assert
            Assert.IsNull(rates);
        }

        [TestMethod]
        public void Test_ExchangeRateGet_DateWithInScopeOfService()
        {
            //arrange
            var date = new DateTime(2014,01,01);
            //act
            var rates = XmlRequestHandler.GetExchangeRateDataByDate(date);
            //assert
            Assert.IsNotNull(rates);
        }

        [TestMethod]
        public void Test_ExchangeRateGet_MaxDateTime()
        {
            //arrange
            var date = DateTime.MaxValue;
            //act
            var rates = XmlRequestHandler.GetExchangeRateDataByDate(date);
            //assert
            Assert.IsNull(rates);
        }

        [TestMethod]
        public void Test_ExchangeRateGet_MinDateTime()
        {
            //arrange
            var date = DateTime.MinValue;
            //act
            var rates = XmlRequestHandler.GetExchangeRateDataByDate(date);
            //assert
            Assert.IsNull(rates);
        }

        [TestMethod]
        public void Test_CalculatedRatesDifference_DateOutOfScopeOfService()
        {
            //arrange
            var date = DateTime.Now;
            //act
            var rates = XmlRequestHandler.CalculatedRatesDifference(date);
            //assert
            Assert.IsNull(rates);
        }

        [TestMethod]
        public void Test_CalculatedRatesDifference_DateWithInScopeOfService()
        {
            //arrange
            var date = new DateTime(2014, 01, 01);
            //act
            var rates = XmlRequestHandler.CalculatedRatesDifference(date);
            //assert
            Assert.IsNotNull(rates);
        }

        [TestMethod]
        public void Test_CalculatedRatesDifference_MaxDateTime()
        {
            //arrange
            var date = DateTime.MaxValue;
            //act
            var rates = XmlRequestHandler.CalculatedRatesDifference(date);
            //assert
            Assert.IsNull(rates);
        }

        [TestMethod]
        public void Test_CalculatedRatesDifference_MinDateTIme()
        {
            //arrange
            var date = DateTime.MinValue;
            //act
            var rates = XmlRequestHandler.CalculatedRatesDifference(date);
            //assert
            Assert.IsNull(rates);
        }
    }
}
