﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UI.Controllers;
using UI.Models;

namespace Test
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Test_HomeController_ValidDate()
        {
            //Arrange
            var controller = new HomeController();

            var model = new DateViewModel();
            model.Date  = new DateTime(2014,01,01);

            //Act
            var result = controller.GetGrid(model) as PartialViewResult;

            //Assert
            Assert.AreEqual("_RateGrid", result.ViewName);
            Assert.IsNotNull(result.Model);
        }

        [TestMethod]
        public void Test_HomeController_DateOutOfScope()
        {
            //Arrange
            var controller = new HomeController();

            var model = new DateViewModel();
            model.Date = new DateTime(2015, 01, 01);

            //Act
            var result = controller.GetGrid(model) as PartialViewResult;

            //Assert
            Assert.AreEqual("_Error", result.ViewName);
            Assert.IsNull(result.Model);
        }
    }
}
