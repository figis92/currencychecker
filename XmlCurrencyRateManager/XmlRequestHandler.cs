﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace XmlCurrencyRateManager
{
    public class XmlRequestHandler
    {
        public static IEnumerable<CurrencyRateModel> GetExchangeRateDataByDate(DateTime date)
        {
            if (date == DateTime.MinValue || date == DateTime.MaxValue)
                return null;

            //ExchangeRates is XML document root
            var serializer = new XmlSerializer(typeof(CurrencyRateModelData), new XmlRootAttribute("ExchangeRates"));

            var uri = "http://www.lb.lt/webservices/ExchangeRates/ExchangeRates.asmx/getExchangeRatesByDate?Date=" + date.ToString("yyyy.MM.dd");

            using (var reader = XmlReader.Create(uri))
            {
                try
                {
                    var result = (CurrencyRateModelData)serializer.Deserialize(reader);
                    return result.CurrencyRates;
                }
                catch
                {
                    // data doesnt exist for this date
                    return null;
                }
            }
        }

        public static IEnumerable<CurrencyRateModel> CalculatedRatesDifference(DateTime date)
        {
            if (date == DateTime.MinValue || date == DateTime.MaxValue)
                return null;

            var requestedDateRates = GetExchangeRateDataByDate(date);
            var previousDateRates = GetExchangeRateDataByDate(date.AddDays(-1));

            if (requestedDateRates == null || previousDateRates == null)
                return null;

            var calculatedRatesDiff = requestedDateRates.Join(previousDateRates,
                previous => previous.Currency,
                requested => requested.Currency,
                (r, a) => new CurrencyRateModel
                {
                    Currency = a.Currency,
                    Quantity = a.Quantity,
                    Rate = a.Rate,
                    Unit = a.Unit,
                    Change = a.Rate - r.Rate
                });

            return calculatedRatesDiff;
        }
    }
}
