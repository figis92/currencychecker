﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace XmlCurrencyRateManager
{
    public class CurrencyRateModel
    {
        [XmlElement(ElementName = "currency")]
        public string Currency { get; set; }

        [XmlElement(ElementName = "quantity")]
        public int Quantity { get; set; }

        [XmlElement(ElementName = "rate")]
        public decimal Rate { get; set; }

        [XmlElement(ElementName = "unit")]
        public string Unit { get; set; }

        public decimal Change { get; set; }
    }

    public class CurrencyRateModelData
    {
        public CurrencyRateModelData()
        {
            CurrencyRates = new List<CurrencyRateModel>();
        }

        [XmlElement("item")]
        public List<CurrencyRateModel> CurrencyRates { get; set; }
    }
}
