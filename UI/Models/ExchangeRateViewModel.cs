﻿namespace UI.Models
{
    public class ExchangeRateViewModel
    {
        public string Currency { get; set; }
        public int Quantity { get; set; }
        public decimal Rate { get; set; }
        public string Unit { get; set; }
        public decimal Change { get; set; }
    }   
}