﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UI.Models
{
    public class DateViewModel
    {
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
    }
}