﻿
//date input if doesnt support HTML5 date input
(function () {
    var elem = document.createElement('input');
    elem.setAttribute('type', 'date');

    if (elem.type === 'text') {
        $('#date').datepicker({
            dateFormat: 'yy-mm-dd',
            maxDate: '2014-12-31'
        });
    }
})();

//value set for date input
$('#date').attr('max', '2014-12-31');


//highlight table change by value
function highlightData() {
    var tableData = $('.table tbody .change');
    $.each(tableData, function (i, val) {
        val = $(val).text();

        if (parseFloat(val) > 0) {
            $(tableData[i]).addClass('success');
        } else if (parseFloat(val) < 0) {
            $(tableData[i]).addClass('danger');
        }
    });
}

function success() {
    highlightData();
}

function error() {
    alert('Input not valid');
}
