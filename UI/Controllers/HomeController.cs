﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using UI.Models;
using XmlCurrencyRateManager;

namespace UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DatePickerForm()
        {
            return PartialView("_DatePickerForm");
        }

        [HttpGet]
        public ActionResult GetGrid(DateViewModel model)
        {
            if(ModelState.IsValid == false)
                return new HttpStatusCodeResult(400, "Bad Request");

            var rateData = XmlRequestHandler.CalculatedRatesDifference(model.Date);

            if (rateData == null)
                return PartialView("_Error");

            Mapper.CreateMap<CurrencyRateModel, ExchangeRateViewModel>();
            IEnumerable<ExchangeRateViewModel> modelCollection = Mapper.Map<IEnumerable<CurrencyRateModel>, IEnumerable<ExchangeRateViewModel>>(rateData);

            return PartialView("_RateGrid", modelCollection.OrderByDescending(m => m.Change));
        }
    }
}